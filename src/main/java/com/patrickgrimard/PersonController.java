package com.patrickgrimard;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by XTL on 8/14/2014.
 */
@RequestMapping("/api/person/**")
@RestController
public class PersonController {

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Person read() {
        return new Person("Ivana", "Todorovic", 27);
    }

    @RequestMapping(method = RequestMethod.POST, produces = {MediaType.ALL_VALUE})
    public String create() {
        return "Kreirana osoba";
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = {MediaType.ALL_VALUE})
    public String delete() {
        return "Izbrisana osoba";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = {MediaType.ALL_VALUE})
    public String update() {
        return "Apdejtovana osoba";
    }

}
